"use strict"

// 1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.

let arr = ["travel", "hello", "eat", "ski", "lift"];

function summArr (arr) {
    return arr.reduce(function (acc, curr){
        if (curr.length > 3) {
            return acc + curr.length;
        } else {
            return acc;
        }
    }, 0);
}
console.log(summArr(arr));

// 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".

// Відфільтрований масив виведіть в консоль.

let users = [
    {
        name: "Ivan",
        age: 25,
        sex: "male"
    },
    {
        name: "Dmitro",
        age: 33,
        sex: "male"
    },
    {
        name: "Margarita",
        age: 25,
        sex: "female"
    }
];

function filterSex (initialArray, sex) {
 const filteredSex = initialArray.filter(
    (user) => user.sex.toLowerCase() === sex.toLowerCase()
    );
    return filteredSex;
}
console.log(filterSex(users, "male"));

// 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
// Технічні вимоги:


// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.

// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

let annyArr = ['hello', 'world', 23, '23', null];

function filterBy (initialArray, type) {
    return initialArray.filter(
        typeArr => typeof typeArr !== type 
        );
}

console.log(filterBy(annyArr, "number" ));
console.log(filterBy(annyArr, "string"));